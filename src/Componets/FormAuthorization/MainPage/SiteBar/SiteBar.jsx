import React from 'react';
import style from './SiteBar.module.css';
import { NavLink } from 'react-router-dom';


export const SiteBar = () => {

    return (
        <div className={style["site-bar"]}>
            <ul className={style["site-bar__list"]}>
                <li className={style["site-bar__item"]}>
                    <NavLink to="Homepage" className={style["site-bar__link"]}>Home</NavLink>
                </li>
            </ul>
        </div>
    );
};

