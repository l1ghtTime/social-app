import React from 'react';
import style from './HomeContent.module.css';

export const HomeContent = (props) => {

    return (
        <div className={style["home-content"]}>
            <h2 className={style["home-content__title"]}>This is site about DC comics</h2>

            <div className={style["home-content__box"]}>
                <div className={style["home-content__image"]} style={
                    {background: `url(${props.imageUrl[0]}) ${"no-repeat"}`,
                    backgroundSize: "cover"
                    
                    }}>

                </div>
                {/* <img src={props.imageUrl[0]}   alt="image"/> */}
            </div>
        </div>
    );
};