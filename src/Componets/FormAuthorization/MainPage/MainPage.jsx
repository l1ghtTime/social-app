import React from 'react';
import { Header } from './Header/Header';
import { SiteBar } from './SiteBar/SiteBar';
import { AreaContent } from './AreaContent/AreaContent';
import style from "./MainPage.module.css"

export const MainPage = (props) => {

    debugger;

    let imageUrl = [
        'https://heroichollywood.com/wp-content/uploads/2020/06/DC_Comics_Dimond_Comics_Distributors.jpg',
        'https://www.syfy.com/sites/syfy/files/styles/1200x680_hero/public/wire/legacy/DC-Universe-Rebirth-1-cover-1_0.jpg'
    ];

    return(
        <React.Fragment>
            <Header />
            <div className={style.content}>
                <SiteBar />
                <AreaContent imageUrl={imageUrl}/>
            </div>
        </React.Fragment>
    );
};



