import React from 'react';
import style from './Header.module.css';
import { NavLink } from 'react-router-dom';


export const Header = () => {

    return (
        <header className={style.header}>
            <div className={style.logo}>
                <NavLink to="Homepage" className={style.logo__link}></NavLink>
            </div>
        </header>
    );
};