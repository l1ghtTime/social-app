import React from 'react';
import style from './AreaContent.module.css';
import { HomeContent } from '../SiteBar/HomeContent/HomeContent';


export const AreaContent = (props) => {

    let regex = RegExp('/Homepage');

    return(

        <div className={style["area-content"]}>
            <HomeContent imageUrl={props.imageUrl}/>
            {<HomeContent /> && !regex.test(window.location.href) ? window.location.href = window.location.href + "Homepage" : null}
        </div>
    );
};
