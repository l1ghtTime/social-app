import Radium from 'radium';
import React from 'react';
import style from './AreaAuthorization.module.css';



const styles = {

    wright: {
        border: '2px solid #81da81',
    },

    error: {
        border: '2px solid #de4141',
    }

}

const AreaAuthorization = (props) => {
    
    const {name, pass, flagName, flagPass, setName, setPass, btnHandler} = props;

    return (
        <div className={style['form-authorization']}>
            <label style={{color: '#0d6b0e'}}>
                Имя:
            <input type="text" required className={style['form-authorization__input-area']} onInput={setName} style={flagName ? styles.wright : null} />
            </label>

            <label style={{color: '#0d6b0e'}}>
                Пароль:
            <input type="password" required name="password" autoComplete="on" className={style['form-authorization__input-area']} onInput={setPass} style={flagPass ? styles.wright : null} />
            </label>

            

            <input type="submit" value="Отправить" className={style['form-authorization__btn']} onClick={name.length > 0 && pass.length > 0 ? btnHandler: null } />
        </div>
    );
};

export default Radium(AreaAuthorization);