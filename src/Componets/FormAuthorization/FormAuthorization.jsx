import React from 'react';
import AreaAuthorization from './AreaAuthorization/AreaAuthorization';
import { MainPage } from './MainPage/MainPage';
import { Route } from 'react-router-dom';

export class FormAuthorization extends React.Component {
    constructor(props) {
        super(props);

        this.areaNameHandler = this.areaNameHandler.bind(this);
        this.areaPassHandler = this.areaPassHandler.bind(this);
        this.btnAuthorizationHandler = this.btnAuthorizationHandler.bind(this);

        this.state = {
            name: '',
            pass: '',
            switchName: false,
            switchPass: false,
            switch: false,
            imageUrl: [],
        }
    }

    areaNameHandler(e) {
        this.setState({
            name: e.target.value,
        }, () => {
            this.setState({
                switchName: this.state.name.length > 0,
            });
        });
    }

    areaPassHandler(e) {
        this.setState({
            pass: e.target.value,
        }, () => {
            this.setState({
                switchPass: this.state.pass.length > 0,
            });
        });
    }

    btnAuthorizationHandler(e) {


        if (!this.state.switch) {
            e.preventDefault();
        }

        window.localStorage.setItem('name', this.state.name);
        window.localStorage.setItem('pass', this.state.pass);

        let regex = RegExp('/Homepage');


        if (!regex.test(window.location.href)) {
            window.location.href = window.location.href + "Homepage";
        }

    }

    render() {

        return (
            <React.Fragment>
                {
                localStorage.getItem('name') ? <MainPage imageUrl={this.state.imageUrl}/> 
                    :
                    <form action="POST">
                        <AreaAuthorization
                            name={this.state.name}
                            pass={this.state.pass}
                            flagName={this.state.switchName}
                            flagPass={this.state.switchPass}
                            setName={this.areaNameHandler}
                            setPass={this.areaPassHandler}
                            btnHandler={this.btnAuthorizationHandler}
                        />
                    </form>
                }
            </React.Fragment>
        );
    }
}

// window.location.href = window.location.href + "Homepage"

/* <MainPage imageUrl={this.state.imageUrl} */

// {/* <MainPage imageUrl={this.state.imageUrl} */}