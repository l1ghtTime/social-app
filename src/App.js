import React from 'react';
import './App.css';
import { FormAuthorization } from './Componets/FormAuthorization/FormAuthorization';
import { Route, Switch } from 'react-router-dom';
import { MainPage } from './Componets/FormAuthorization/MainPage/MainPage';

function App() {
  return (

    <Switch>
      <Route exact path="/" component={FormAuthorization} />
      <Route path="/Homepage" component={MainPage} />
    </Switch>
  );
}

export default App;



